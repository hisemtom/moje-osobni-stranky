$(document).ready(function () {

    $(".hiden").hide();
    $(".logModal #closeModal").mouseenter(function () {
        $(".hiden").show();
    });
    $(".logModal #closeModal").mouseleave(function () {
        $(".hiden").hide();
    });
    $(".regModal #closeModal").mouseenter(function () {
        $(".hiden").show();
    });
    $(".regModal #closeModal").mouseleave(function () {
        $(".hiden").hide();
    });
    $('.js-hobbyContent').hide();
    $('.js-hobbyContent:first').show();

    $('h2').click(function () {
        var contentId = $(this).data('hobbyid');

        $('.js-hobbyContent').hide();
        $(contentId).show();
    });
    $('#myModalNew').click(
            function (event) {
                event.preventDefault();
                $("#modal-background").fadeToggle();
                $(".logModal").addClass("show");
                $("#modal-background").click(function (e) {
                    if (e.target !== this) {
                        return true;
                    } else
                        $(".logModal").removeClass("show");
                    $("#modal-background").fadeOut("slow");
                    return false;

                });
                $("#closeModal").click(function (e) {
                    if (e.target !== this) {
                        return true;
                    } else
                        $(".logModal").removeClass("show");
                    $("#modal-background").fadeOut("slow");
                    return false;

                });
            });
    $('.registration').click(
            function (event) {
                event.preventDefault();
                $("#modal-background").fadeToggle();
                $(".regModal").fadeToggle();
                $("#modal-background").click(function (e) {
                    if (e.target !== this) {
                        return true;
                    } else
                        $(".regModal").fadeOut("slow");
                    $("#modal-background").fadeOut("slow");
                    return false;

                });
                $(".closeReg").click(function (e) {
                    if (e.target !== this) {
                        return true;
                    } else
                        $(".regModal").fadeOut("slow");
                    $("#modal-background").fadeOut("slow");
                    return false;

                });

            });
    $('form[name="login"]').submit(function (e) {
        var $form = $(this);
        e.preventDefault();

        $.ajax({
            url: 'logger.php',
            method: "POST",
            dataType: "json",
            data: {
                logNick: $form.find('INPUT[name = "logNick"]').val(),
                logPassword: $form.find('INPUT[name="logPassword"]').val(),
                logYear: $form.find('INPUT[name="logYear"]').val()
            }
        }).done(function (response) {
            if (response.message === '') {
                $('.logModal').html('<div class="red" style="text-align: center">' + response.success + '</div>');
                $('.logModal').hide(8000);
                $("#modal-background").hide();
                setTimeout("location.href = 'index.php'", 3000);
            } else {
                var message = response.message;
                var key = response.key;
                if (key === 'spam') {
                    key = '#logSpam';
                } else if (key === 'logNick') {
                    key = '#logNick';
                } else if (key === 'logPassword') {
                    key = '#logPass1';
                }
                $('.loginMessage').remove();
                var txt = $('<div class="red loginMessage"></div>').text(message);
                $(key).after(txt);
            }
        });

    });
    $('form[name="user"]').submit(function (e) {
        var $form = $(this);
        e.preventDefault();

        $.ajax({
            url: 'register.php',
            method: "POST",
            dataType: "json",
            data: {
                firstName: $form.find('INPUT[name="firstName"]').val(),
                lastName: $form.find('INPUT[name="lastName"]').val(),
                nick: $form.find('INPUT[name = "nick"]').val(),
                mail: $form.find('INPUT[name="mail"]').val(),
                password: $form.find('INPUT[name="password"]').val(),
                password_again: $form.find('INPUT[name="password_again"]').val(),
                year: $form.find('INPUT[name="year"]').val()
            }
        }).done(function (response) {
            if (response.message === '') {
                $('.regModal').html('<div class="red" style="text-align: center">' + response.success + '</div>');
                $('.regModal').hide(8000);
                $("#modal-background").hide();
                setTimeout("location.href = 'index.php'", 3000);
            } else {
                var message = response.message;
                var key = response.key;
                if (key === 'spam') {
                    key = '#regSpam';
                } else if (key === 'nick') {
                    key = '#regNick';
                } else if (key === 'password') {
                    key = '#regPass1';
                } else if (key === 'password_again') {
                    key = '#regPass2';
                } else if (key === 'mail') {
                    key = '#regMail';
                } else if (key === 'firstName' || key === 'name') {
                    key = '#regName1';
                } else if (key === 'lastName') {
                    key = '#regName2';
                }

                $('.regMessage').remove();
                var txt = $('<div class="red regMessage"></div>').text(message);
                $(key).after(txt);
            }
        });

    });
    $('form[name="firstNameForm"').submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        var $nameChange = $form.attr('name').replace('Form', 'Profile');
        ajaxChangeProfile($form, $nameChange);
    });
    $('form[name="nickForm"').submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        var $nameDataChange = $form.attr('name').replace('Form', 'Profile');
        ajaxChangeProfile($form, $nameDataChange);
    });
    $('form[name="mailForm"').submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        var $nameDataChange = $form.attr('name').replace('Form', 'Profile');
        ajaxChangeProfile($form, $nameDataChange);
    });
    $('form[name="passwordForm"').submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        var $nameDataChange = $form.attr('name').replace('Form', 'Profile');
        $('#warning').empty();
        $.ajax({
            url: 'changeProfile.php',
            method: "POST",
            dataType: "json",
            data: {nameChange: $form.find('INPUT[name="passwordProfile"]').val() + $nameDataChange,
                again: $form.find('INPUT[name="password_againProfile"]').val()}
        }).done(function (response) {
            if (response.success === true) {
                $form.after("<p id='warning' class='red'><b>Požadovaná změna byla uložena</b></p>");
                setTimeout("location.href = 'index.php?id=9'", 3000);
            } else {
                $form.after("<p id='warning' class='red'><b>" + (response.message) + "</b></p>");
                setTimeout("location.href = 'index.php?id=9'", 3000);
            }
        });
    });
    function ajaxChangeProfile($form, $nameDataChange) {
        $('#warning').empty();
        $.ajax({
            url: 'changeProfile.php',
            method: "POST",
            dataType: "json",
            data: {nameChange: $form.find('INPUT[name="' + $nameDataChange + '"]').val() + $nameDataChange}
        }).done(function (response) {
            if (response.success === true) {
                $form.after("<p id='warning' class='red'><b>Požadovaná změna byla uložena</b></p>");
                setTimeout("location.href = 'index.php?id=9'", 3000);
            } else {
                $form.after("<p id='warning' class='red'><b>" + (response.message) + "</b></p>");
                setTimeout("location.href = 'index.php?id=9'", 3000);
            }
        });
    }
    $('#firstNameProfile').hide();
    $('#firstNameProfile').after('<input type="checkbox" id="checkFNP" name="checkChange[]" form="changeChecked" value="1"><span class="red">Změnit jméno</span>');
    $('#btnFNP').hide();
    $('#nickProfile').hide();
    $('#nickProfile').after('<input type="checkbox" id="checkNP" name="checkChange[]" form="changeChecked" value="2"><span class="red">Změnit nick</span>');
    $('#btnNP').hide();
    $('#passwordProfile').hide();
    $('#password_againProfileLabel').hide();
    $('#password_againProfile').hide();
    $('#passwordProfile').after('<input type="checkbox" id="checkPP" name="checkChange[]" form="changeChecked" value="4"><span class="red">Změnit heslo</span>');
    $('#btnMP').hide();
    $('#mailProfile').hide();
    $('#mailProfile').after('<input type="checkbox" id="checkMP" name="checkChange[]" form="changeChecked" value="3"><span class="red">Změnit mail</span>');
    $('#btnPP').hide();
    $('#checkFNP').click(function () {
        $('#deleteUser').hide().after('<a href="index.php?id=9" style="text-decoration: none;"><div id="return">Zpět</div></a>');
        if ($("#checkFNP").prop('checked')) {
            $('input[type="checkbox"]').hide();
            $('.red').hide();
            $('#firstNameProfile').show();
            $('#btnFNP').show();
        }

    });
    $('#checkNP').click(function () {
        $('#deleteUser').hide().after('<a href="index.php?id=9" style="text-decoration: none;"><div id="return">Zpět</div></a>');
        if ($("#checkNP").prop('checked')) {
            $('input[type="checkbox"]').hide();
            $('.red').hide();
            $('#nickProfile').show();
            $('#btnNP').show();
        }
    });
    $('#checkPP').click(function () {
        $('#deleteUser').hide().after('<a href="index.php?id=9" style="text-decoration: none;"><div id="return">Zpět</div></a>');
        if ($("#checkPP").prop('checked')) {
            $('input[type="checkbox"]').hide();
            $('.red').hide();
            $('#passwordProfile').show();
            $('#password_againProfileLabel').show();
            $('#password_againProfile').show();
            $('#btnPP').show();
        }
    });
    $('#checkMP').click(function () {
        $('#deleteUser').hide().after('<a href="index.php?id=9" style="text-decoration: none;"><div id="return">Zpět</div></a>');
        if ($("#checkMP").prop('checked')) {
            $('input[type="checkbox"]').hide();
            $('.red').hide();
            $('#mailProfile').show();
            $('#btnMP').show();
        }
    });
    $('#deleteUser').click(function (e) {
        e.preventDefault();
        $('#myProfile').empty().html('<div class="red" style="font-size:30px">Opravdu chcete smazat svůj profil?</div>' +
                '<button id="deleteYes">Ano</button> <button id="deleteNo">Ne</button>');
        $('#deleteYes').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: 'changeProfile.php',
                method: "POST",
                dataType: "json",
                data: {nameChange: 'delete'}
            }).done(function (response) {
                if (response.success === true) {
                    $('.regModal').html("<p class='red'><b>Profil byl úspěšně smazán</b></p>");
                    $('.regModal').hide(5000);
                    $("#modal-background").hide();
                    setTimeout("location.href = 'index.php'", 3000);
                } else {
                    $('.regModal').html("<p class='red'><b>Spojení s databází selhalo</b></p>");
                    $('.regModal').hide(5000);
                    $("#modal-background").hide();
                    setTimeout("location.href = 'index.php?id=9'", 3000);
                }
            });
        });
        $('#deleteNo').click(function (e) {
            e.preventDefault();
            setTimeout("location.href = 'index.php?id=9'", 1000);
        });
    });
    $('#menuButton').click(function(){
        $('#menuInHiddenMenu li').toggleClass('show');
    });
});










