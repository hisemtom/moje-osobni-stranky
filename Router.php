<?php

//use Page;
//use TextControll;

class Router {

    /**
     * 
     * @var array
     */
    private $post = [];

    /**
     * 
     * @var TextControll 
     */
    private $textCtrl;

    /**
     * 
     * @var array
     */
    private $dataFromForm = [];

    /**
     * 
     * @param Page\Controller 
     */
    private $pageCtrl;

    /**
     * 
     * @param Page\Controller $pageCtrl
     */
    public function __construct(Page\Controller $pageCtrl) {
        $this->pageCtrl = $pageCtrl;
        $this->post = $this->pageCtrl->getTextCtrl()->getPost();
        $this->textCtrl = $this->pageCtrl->getTextCtrl();
        $this->dataFromForm = ['logNick' => htmlspecialchars(trim(($this->post['logNick']) ?? '')),
            'logYear' => htmlspecialchars(trim(($this->post['logYear']) ?? '')),
            'logPassword' => htmlspecialchars(trim(($this->post['logPassword']) ?? '')),
            'name' => htmlspecialchars(trim(($this->post['firstName']) ?? ' ')),
            'firstName' => htmlspecialchars(trim(($this->post['firstName']) ?? '')),
            'lastName' => htmlspecialchars(trim(($this->post['lastName']) ?? '')),
            'nick' => htmlspecialchars(trim(($this->post['nick']) ?? '')),
            'password' => htmlspecialchars(trim(($this->post['password']) ?? '')),
            'password_again' => htmlspecialchars(trim(($this->post['password_again']) ?? '')),
            'mail' => htmlspecialchars(trim(($this->post['mail']) ?? '')),
            'year' => htmlspecialchars(trim(($this->post['year']) ?? '')),
            'emailText' => htmlspecialchars(trim(($this->post['emailText']) ?? '')),
            'emailFromContact' => htmlspecialchars(trim(($this->post['emailFromContact']) ?? '')),
            'emailYear' => htmlspecialchars(trim(($this->post['emailYear']) ?? '')),
            'firstNameProfile' => htmlspecialchars(trim(($this->post['firstNameProfile']) ?? '')),
            'nickProfile' => htmlspecialchars(trim(($this->post['nickProfile']) ?? '')),
            'mailProfile' => htmlspecialchars(trim(($this->post['mailProfile']) ?? '')),
            'passwordProfile' => htmlspecialchars(trim(($this->post['passwordProfile']) ?? '')),
            'password_againProfile' => htmlspecialchars(trim(($this->post['password_againProfile']) ?? '')),
            'checkChange' => $this->post['checkChange'] ?? [],
        ];
    }

    /**
     * 
     * @return array
     */
    public function getPost(): array {
        return $this->post;
    }

    /**
     * 
     * @return TextControll
     */
    public function getTextCtrl(): TextControll {
        return $this->textCtrl;
    }

    /**
     * 
     * @return array
     */
    public function getDataFromForm(): array {
        return $this->dataFromForm;
    }

    /**
     * 
     * @return Page\Controller
     */
    public function getCtrl(): Page\Controller {
        return $this->pageCtrl;
    }

    /**
     * 
     * @return type
     */
    public function getMessage() {
        return $this->pageCtrl->getView()->setMessage($this->textCtrl->getMessage());
    }

    public function postControll() {
        if (!empty($this->dataFromForm['nick'])) {
            $this->pageCtrl->setSuccess($this->pageCtrl->getModel()->getReg($this->dataFromForm, $this->textCtrl));
        } elseif (!empty($this->dataFromForm['logNick'])) {
            $this->pageCtrl->setSuccess($this->pageCtrl->getModel()->getLogStatus($this->dataFromForm, $this->textCtrl));
        } elseif (!empty($this->dataFromForm['emailFromContact'])) {
            $this->pageCtrl->setSuccess($this->pageCtrl->getModel()->getMail($this->dataFromForm, $this->textCtrl));
        } elseif (!empty($this->dataFromForm['nickProfile']) || !empty($this->dataFromForm['mailProfile']) || !empty($this->dataFromForm['passwordProfile']) || !empty($this->dataFromForm['firstNameProfile'])) {
            $this->getCtrl()->getChange($this->getDataFromForm(),$this->getTextCtrl());
        }
    }

    /**
     * 
     * @param type $id
     */
    public function getLogout($id) {
        if ((int) $id === 8) {
            unset($_SESSION['nick']);
            session_destroy();
        }
    }

}
