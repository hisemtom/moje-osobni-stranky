<?php

namespace Page;

use TextControll;

class Controller {

    /**
     * 
     * @var int
     */
    private $id;

    /**
     *
     * @var array
     */
    private $post;

    /**
     *
     * @var Module
     */
    public $Model;

    /**
     *
     * @var View
     */
    private $View;

    /**
     *
     * @var  TextControll 
     */
    private $textCtrl;

    /**
     *
     * @var string
     */
    private $success = '';

    public function __construct(TextControll $textCtrl) {
        $this->id = $textCtrl->getId();
        $this->post = $textCtrl->getPost();
        $this->pageModel = new Model();
        $this->pageView = new View();
        $this->textCtrl = $textCtrl;
    }

    /**
     * 
     * @return array
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return array
     */
    public function getPost(): array {
        return $this->post;
    }

    /**
     * 
     * @return TextControll
     */
    public function getTextCtrl(): TextControll {
        return $this->textCtrl;
    }

    /**
     * 
     * @param string $success
     */
    public function setSuccess($success) {
        $this->success = $success;
    }

    /**
     * 
     * @return Model
     */
    function getModel(): Model {
        return $this->pageModel;
    }

    /**
     * 
     * @return View
     */
    function getView(): View {
        return $this->pageView;
    }

    /**
     * 
     * @return string
     */
    function getSuccess(): string {
        return $this->success;
    }

    /**
     * 
     * @return string
     */
    public function getPage(): string {
        return $this->pageView->viewFinalPage($this->getId(), $this->pageModel->getMenuData(), $this->pageModel->getHobbies(), $this->success, $this->getModel()->getProfilePage());
    }

    /**
     * 
     * @return array
     */
    public function getMenu(): array {

        return $this->getModel()->getMenuData();
    }

    /**
     * 
     * @return string
     */
    public function getTitle(): string {

        return $this->pageModel->getTitle($this->id);
    }

    /**
     * 
     * @return string
     */
    public function getLogo(): string {

        return $this->pageView->viewLogo($this->pageModel->getLogo());
    }

    /**
     * 
     * @return string
     */
    public function getLoginHref(): string {

        return isset($_SESSION['nick']) ? $this->pageView->viewLogout() : $this->pageView->viewLogin()['login'] . $this->pageView->viewLogin()['registration'];
    }

    public function getLogMail() {
        return $this->getView()->setLogMail($this->getModel()->getLogMail());
    }

    public function getProfilePage() {
        $this->getView()->getProfilePage($this->getModel()->getProfilePage());
    }

    public function getChange(array $nameProfile) {
        $this->getModel()->getChange($nameProfile, $this->getTextCtrl());
    }

}
