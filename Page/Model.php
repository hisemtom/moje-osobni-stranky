<?php

namespace Page;

use TextControll;


class Model {

    /**
     *
     * @var \mysqli 
     */
    private $linkMySQL;

    public function __construct() {
        $this->linkMySQL = $this->getConnection('myfirstweb');
    }

    /**
     * 
     * @return \mysqli
     */
    public function getLink() {

        return $this->linkMySQL;
    }

    /**
     * 
     * @param string $databaseName
     * @return \mysqli
     */
    protected function getConnection(string $databaseName) {

        $connection = new \mysqli('localhost', 'root', 'root', $databaseName);
        if (!$connection) {
            die('Nelze připojit k databázi');
            $connection->close();
            exit();
        }
        $connection->query("SET NAMES utf8");

        return $connection;
    }

    /**
     * 
     * @return array
     */
    public function getMenu(): array {
        $query = $this->getLink()->query('SELECT id, menu, article,url FROM mymenu');
        $rows = $query->fetch_all(MYSQLI_ASSOC);
        $query->free();

        return $rows;
    }

    /**
     * 
     * @return array
     */
    public function getHobbies(): array {
        $query = $this->getLink()->query('SELECT url,article, nameHobby FROM hobby');
        $rows = $query->fetch_all(MYSQLI_ASSOC);
        $query->free();

        return $rows;
    }

    /**
     * 
     * @param int $id
     * @return string
     */
    public function getTitle(int $id): string {

        return $this->getMenuData($this->getMenu())[$id]['menu'];
    }

    /**
     * 
     * @return array
     */
    public function getMenuData(): array {
        $rows = $this->getMenu();
        foreach ($rows as $row) {
            $returnPages[$row['id']] = [
                'id' => $row['id'],
                'link' => '<a href="index.php?id=' . $row['id'] . '"><li class="nav-item active">' . $row['menu'] . '</li></a>',
                'content' => $row['article'],
                'menu' => $row['menu'],
                'url' => $row['url'],
            ];
        }

        return $returnPages;
    }

    /**
     * 
     * @return string
     */
    public function getLogo(): string {
        $logUser = $_SESSION['nick'] ?? 'guest';

        return $logUser;
    }

    /**
     * 
     * @param array $dataFM
     * @param TextControler $textCtrl
     * @return string
     */
    public function getMail(array $dataFM, TextControll $textCtrl): string {
        $success = '';
        if ($textCtrl->controllSpam($dataFM['emailYear'])) {
            
        } elseif ($textCtrl->controllMailAdress($dataFM['emailFromContact'])) {
            
        } else {
            if ($this->sendMail($dataFM)) {
                $success = 'E-mail byl úspěšně odeslán';
            } else {
                $success = 'E-mail se nepodařílo odeslat. ';
            }
        }

        return $success;
    }

    /**
     * 
     * @param array $dataFM
     * @return bool
     */
    public function sendMail(array $dataFM): bool {
        $headers = "From: ".htmlspecialchars(trim($dataFM['emailFromContact']))."\r\n";
        $to = 'tomas.hisem@seznam.cz';
        $subject = "Test Mailu";
        $message = htmlspecialchars(trim($dataFM['emailText']));
        unset($_POST);

        return mail($to, $subject, $message, $headers);
    }

    /**
     * 
     * @param array $dataFF
     * @param TextControler $textCtrl
     * @return string
     */
    public function getReg(array $dataFF, TextControll $textCtrl): string {
        $success = '';
        $condition1 = $textCtrl->controllName($dataFF['name'], $dataFF);
        $condition2 = $textCtrl->controllMailAdress($dataFF['mail']);
        $condition3 = $textCtrl->controllPassword($dataFF['password'], $dataFF['password_again']);
        $condition4 = $textCtrl->controllSpam($dataFF['year']);
        if (!$condition1 || !$condition2 || !$condition3 || !$condition4) {
            if (!($textCtrl->controllNick((int) ($this->linkMySQL->query("SELECT 1 FROM `users` WHERE `nick`='" . $dataFF['nick'] . "' LIMIT 1")->fetch_row()[0])))) {
                $this->saveUser($dataFF);
                $_SESSION['nick'] = $dataFF['nick'];
                $success = 'Registrace proběhla úspěšně<br>Vítej uživateli <b>' . $dataFF["nick"] . '</b>';
            }       
        }
        return $success;
    }

    /**
     * 
     * @param array $dataFF
     */
    public function saveUser(array $dataFF) {
        $name = $dataFF['name'];
        $nick = $dataFF['nick'];
        //$picturePath = $dataFF['picturePath'] ?? ' ';
        $password = password_hash($dataFF['password'], PASSWORD_DEFAULT);
        $mail = $dataFF['mail'];
        $query = "INSERT INTO `users` (`id`, `name`, `nick`, `password`, `mail`) VALUES(NULL,'" . $name . "','" . $nick . "','" . $password . "','" . $mail . "')";
        $this->linkMySQL->query($query);
    }

    /**
     * 
     * @param array $dataFL
     * @param TextControler $textCtrl
     * @return string
     */
    public function getLogStatus(array $dataFL, TextControll $textCtrl): string {
        $success = '';
        $existNick = $this->getLink()->query("SELECT 1 FROM users WHERE nick='" . $dataFL['logNick'] . "' LIMIT 1")->fetch_row()[0];
        if ($textCtrl->controllSpam($dataFL['logYear'])) {
            
        } elseif ($textCtrl->controllLogNick((int) $existNick)) {
            
        } else {
            if ($textCtrl->controllLogPassword($dataFL['logPassword'], $this->getLink()->query("SELECT password FROM users WHERE nick='" . $dataFL['logNick'] . "' LIMIT 1")->fetch_row()[0])) {
                $success = 'Přihlášení se nezdařilo';
            } else {
                $_SESSION['nick'] = $dataFL['logNick'];
                $success = 'Přihlášení proběhlo úspěšně<br>Vítej zpět <b>' . $dataFL['logNick'] . '</b>';
            }
        }

        return $success;
    }

    /**
     * 
     * @return string
     */
    public function getLogMail(): string {
        $nick = $_SESSION['nick'] ?? '';
        if ($nick !== '') {

            return $this->linkMySQL->query("SELECT `mail` FROM `users` WHERE `nick`='" . $nick . "' LIMIT 1")->fetch_row()[0];
        } else {
            return '@';
        }
    }

    /**
     * 
     * @return array
     */
    public function getProfilePage(): array {
        $arr = [];
        $return = (isset($_SESSION['nick'])) ? $this->linkMySQL->query('SELECT * FROM `users` WHERE `nick`="' . $_SESSION['nick'] . '"')->fetch_assoc() : $arr;
        return $return;
    }

    /**
     * 
     * @param array $inValidProfile
     * @param TextControll $textCtrl
     * @return boolean
     */
    public function getChange(array $inValidProfile, TextControll $textCtrl) {
        if (isset($inValidProfile['nameChange'])){
            $inValid = $this->change($inValidProfile);
        } else {
            $inValid = $inValidProfile;
        } 
        $query = 'UPDATE `users` SET ';
        $query2 = ' WHERE `nick` ="' . $_SESSION['nick'] . '"';
        $success = FALSE;
        if ($inValid['firstNameProfile'] !== '') {
            $success = $this->validName($query, $query2, $inValid, $textCtrl);
        } elseif ($inValid['nickProfile'] !== '') {
            $success = $this->validNick($query, $query2, $inValid, $textCtrl);
        } elseif ($inValid['mailProfile'] !== '') {
            $success = $this->validMail($query, $query2, $inValid, $textCtrl);
        } elseif ($inValid['passwordProfile'] !== '') {
            $success = $this->validPassword($query, $query2, $inValid, $textCtrl);
        } else {
            $textCtrl->setMesage('password', 'Pole je prázné');
        }

        return $success;
    }

    /**
     * 
     * @param array $inValidProfile
     * @return array
     */
    private function change(array $inValidProfile): array {      
        $inValid['firstNameProfile'] = (strpos($inValidProfile['nameChange'], 'firstNameProfile')) ? trim(str_replace('firstNameProfile', '', $inValidProfile['nameChange'])) : '';
        $inValid['nickProfile'] = (strpos($inValidProfile['nameChange'], 'nickProfile')) ? trim(str_replace('nickProfile', '', $inValidProfile['nameChange'])) : '';
        $inValid['mailProfile'] = (strpos($inValidProfile['nameChange'], 'mailProfile')) ? trim(str_replace('mailProfile', '', $inValidProfile['nameChange'])) : '';
        $inValid['passwordProfile'] = (strpos($inValidProfile['nameChange'], 'passwordProfile')) ? trim(str_replace('passwordProfile', '', $inValidProfile['nameChange'])) : '';
        $inValid['password_againProfile'] = $inValidProfile['nameChange'] ?? '';

        return $inValid;
    }

    /**
     * 
     * @return bool
     */
    public function deleteUser(): bool {
        $success = FALSE;
        $success = $this->linkMySQL->query('DELETE FROM `users` WHERE `nick` ="' . $_SESSION['nick'] . '"');
        unset($_SESSION['nick']);

        return $success;
    }

    /**
     * 
     * @param string $query
     * @param string $query2
     * @param array $inValid
     * @param TextControll $textCtrl
     * @return bool
     */
    public function validName(string $query, string $query2, array $inValid, TextControll $textCtrl): bool {
        $success = $textCtrl->controllName($inValid['firstNameProfile']);
        if (!$success) {
            $this->linkMySQL->query($query . '`name`="' . $inValid['firstNameProfile'] . '"' . $query2);
            $success = TRUE;
        } else {
            $success = FALSE;
        }

        return $success;
    }

    /**
     * 
     * @param string $query
     * @param string $query2
     * @param array $inValid
     * @param TextControll $textCtrl
     * @return bool
     */
    public function validPassword(string $query, string $query2, array $inValid, TextControll $textCtrl): bool {
        $success = $textCtrl->controllPassword($inValid['passwordProfile'], $inValid['password_againProfile']);
        if (!$success) {
            $this->linkMySQL->query($query . '`password` ="' . password_hash($inValid['passwordProfile'], PASSWORD_DEFAULT) . '"' . $query2);
            $success = TRUE;
        } else {
            $success = FALSE;
        }

        return $success;
    }

    /**
     * 
     * @param string $query
     * @param string $query2
     * @param array $inValid
     * @param TextControll $textCtrl
     * @return bool
     */
    public function validNick(string $query, string $query2, array $inValid, TextControll $textCtrl): bool {

        $exist = $this->linkMySQL->query("SELECT 1 FROM users WHERE nick='" . $inValid['nickProfile'] . "' LIMIT 1")->fetch_row()[0];
        $success = $textCtrl->controllNick((int) $exist);
        if (!$success) {
            $this->linkMySQL->query($query . '`nick` ="' . $inValid['nickProfile'] . '"' . $query2);
            $success = TRUE;
            $_SESSION['nick'] = $inValid['nickProfile'];
        } else {
            $success = FALSE;
        }

        return $success;
    }

    /**
     * 
     * @param string $query
     * @param string $query2
     * @param array $inValid
     * @param TextControll $textCtrl
     * @return bool
     */
    public function validMail(string $query, string $query2, array $inValid, TextControll $textCtrl): bool {
        $success = $textCtrl->controllMailAdress($inValid['mailProfile']);
        if (!$success) {
            $this->linkMySQL->query($query . '`mail` ="' . $inValid['mailProfile'] . '"' . $query2);
            $success = TRUE;
        } else {
            $success = FALSE;
        }

        return $success;
    }

}
