<?php

session_set_cookie_params(0);
session_start();
//Load composer's autoloader
require 'vendor/autoload.php';

mb_internal_encoding("UTF-8");
spl_autoload_register(function ($class) {

    require ("$class.php");
});

$id = $_GET['id'] ?? '1';
$router = new Router(new \Page\Controller(new TextControll($id, $_POST)));
