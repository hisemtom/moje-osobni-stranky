<?php

require_once ('api.php');

$arrayChanges = [
    'nameChange' => $_POST['nickProfile'] ?? ($_POST['passwordProfile'] ?? ($_POST['mailProfile'] ?? ($_POST['firstNameProfile'] ?? $_POST['nameChange']))),
    'again' => $_POST['again']??'',];

$return = [];

$success = $router->getCtrl()->getModel()->getChange($arrayChanges, $router->getCtrl()->getTextCtrl());
$messages = $router->getTextCtrl()->getMessage();
if ($arrayChanges['nameChange'] === 'delete') {
    $success = $router->getCtrl()->getModel()->deleteUser();
}
$return['success'] = $success;
$return['message'] = '';
foreach ($messages as $key => $message) {
    if ($message !== '') {
        $return['message'] = $message;
    }
}
header('Content-type: application/json; charset=utf-8');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

echo json_encode($return);


