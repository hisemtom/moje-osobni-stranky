<?php

use PHPUnit\Framework\TestCase;


require_once dirname(__FILE__) . '/../TextControll.php';
require_once dirname(__FILE__) . '/../Router.php';
/**
 * 
 * @covers Router
 */
class RouterTest extends TestCase { 

    /**
     *
     * @var Page\Controller 
     */
    private $pageCtrlMock;
    
    /**
     *
     * @var Router 
     */
    private $router;

    public function setUp() {
        
        $textCtrl = $this->getMockBuilder(TextControll::class)
                ->setConstructorArgs(["1", []])
                ->getMock();
        
        $textCtrl->expects($this->any())
                ->method('getPost')
                ->will($this->returnValue([]));
        
        $textCtrl->expects($this->once())
                ->method('getId')
                ->will($this->returnValue("1"));
        
        //$textCtrl = new TextControll("1", []);
        $this->pageCtrlMock = $this->getMockBuilder(Page\Controller::class)
                ->setConstructorArgs([$textCtrl])
                ->getMock();
        
        $this->pageCtrlMock->expects($this->any())
                ->method('getPost')
                ->will($this->returnValue([]));
        
        $this->pageCtrlMock->expects($this->any())
                ->method('getTextCtrl')
                ->will($this->returnValue($textCtrl));
        
        $this->router = new Router($this->pageCtrlMock);
    }
    
    /** 
     * @return array
     */
    public function intLogoutProvider() {
        session_start();
        $_SESSION['nick'] = 'nick';
        $sessionNick = $_SESSION['nick'];
        return [
            [0, $sessionNick, FALSE],
            [8, '', TRUE],
        ];
    }

    public function testGetPost() {
        $post = $this->router->getPost();
        $this->assertEquals($post, [], 'nic');
    }
    
    /**
     * @param int $id
     * @paran string $sessionNick
     * @param bool $expected
     * @dataProvider intLogoutProvider
     */
    public function testGetLogout(int $id,string $sessionNick, bool $expected) {
        $this->router->getLogout($id);
        $this->assertEquals($expected, empty($sessionNick));
    }

}
