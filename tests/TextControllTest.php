<?php
use PHPUnit\Framework\TestCase;

require_once dirname(__FILE__) . '/../TextControll.php';
/**
 * 
 * @covers TextControll
 */
class TextControllTest extends TestCase {
    
    /**
     *
     * @var TextControll 
     */
    private $textCtrl;
    
    public function setUp() {
        $this->textCtrl = new TextControll(1,[]);
    }

    /** 
     * @return array
     */
    public function stringDataProvider() {
        return [
            ['neděle', 'nedele'],
            ['Tomáš Dvořák', 'Tomas Dvorak']
        ];
    }
    
    /** 
     * @return array
     */
    public function stringYearProvider() {
        return [
            ['2017', '2017' !== date('Y')],
            ['2016', '2016' !== date('Y')],
            ['2018', '2018' !== date('Y')],
        ];
    }
    
    /** 
     * @return array
     */
    public function stringPasswordProvider() {
        return [
            ['heslo', 'password', TRUE],
            ['trtr', 'trtr', TRUE],
            ['password', '', TRUE],
            ['password', 'password', FALSE],
        ];
    }
    
    /** 
     * @return array
     */
    public function intNickProvider() {
        return [
            [0, FALSE],
            [1, TRUE],
        ];
    }
    
    /** 
     * @return array
     */
    public function stringMailProvider() {
        return [
            ['nějakýmail@seznam.cz', TRUE],
            ['mailseznam.cz', TRUE],
            ['$mail@email.com', TRUE],
            ['mail@mail.com', FALSE],
        ];
    }
    
    /** 
     * @return array
     */
    public function stringNameProvider() {
        return [
            ['Tomáš', TRUE],
            ['TomášHisem', TRUE],
            ['Tomáš hisem', TRUE],
            ['tomáš Hisem', TRUE],
            ['Tomáš Hisem', FALSE],
            ['Tomáš Hisem Novotný', FALSE],
        ];
    }
    
    /**
     * 
     * @param string $someText
     * @param string $expected
     * @dataProvider stringDataProvider
     */
    public function testUtfToAscii(string $someText, string $expected) {  
        $someTextAscii = $this->textCtrl->utfToAscii($someText);
     $this->assertEquals($someTextAscii, $expected) ;  
    }
    
    /**
     * @param string $year
     * @param bool $expected
     * @dataProvider stringYearProvider
     */
    public function testControllSpam(string $year, bool $expected) {
        $controllSpam = $this->textCtrl->controllSpam($year);
        $this->assertEquals($controllSpam, $expected);
    }
    
    /**
     * @param string $pass
     * @param string $apass
     * @param bool $expected
     * @dataProvider stringPasswordProvider
     */
    public function testControllPassword(string $pass, string $apass, bool $expected) {
        $controllPassword = $this->textCtrl->controllPassword($pass, $apass);
        $this->assertEquals($controllPassword, $expected);
    }
    
    /**
     * @param string $pass
     * @param string $apass
     * @param bool $expected
     * @dataProvider stringPasswordProvider
     */
    public function testControllLogPassword(string $pass, string $apass, bool $expected) {
        $controllPassword = $this->textCtrl->controllLogPassword($pass, password_hash($apass, PASSWORD_DEFAULT));
        $this->assertEquals($controllPassword, $expected);
    }
    
    /**
     * @param int $exist
     * @paran bool $expected
     * @dataProvider intNickProvider
     */
    public function testControllNick(int $exist, bool$expected) {
        $controllNick = $this->textCtrl->controllNick($exist);
        $this->assertEquals($controllNick, $expected);
    }
    
    /**
     * @param int $exist
     * @paran bool $expected
     * @dataProvider intNickProvider
     */
    public function testControllLogNick(int $exist, bool$expected) {
        $controllNick = $this->textCtrl->controllLogNick($exist);
        $this->assertEquals($controllNick, !$expected);
    }
    
    /**
     * @param string $mail
     * @param bool $expected
     * @dataProvider stringMailProvider
     */
    public function testControllMailAdress(string $mail, bool $expected) {
        $controllMail = $this->textCtrl->controllMailAdress($mail);
        $this->assertEquals($controllMail, $expected);
    }
    
    /**
     * @param string $name
     * @param bool $expected
     * @dataProvider stringNameProvider
     */
    public function testControllName(string $name, bool $expected) {
        $controllName = $this->textCtrl->controllName($name);
        $this->assertEquals($controllName, $expected);
    }
}
