<?php

//kontrola dat z registračních formulářů
class TextControll {

    /**
     * 
     * @var array
     */
    private $id;

    /**
     * 
     * @param array
     */
    private $post;

    /**
     *
     * @var array
     */
    private $mesage = ['firstName' => '',
        'lastName' => '',
        'nick' => '',
        'mail' => '',
        'password' => '',
        'password_again' => '',
        'spam' => '',
        'logNick' => '',
        'logPassword' => '',
        'emailText' => '',];

    public function __construct(string $id, array $post) {
        $this->id = $id;
        $this->post = $post;
    }

    /**
     * 
     * @return int
     */
    public function getId() {
        return (int) $this->id;
    }

    /**
     * 
     * @return array
     */
    public function getPost() {
        return $this->post;
    }

    /**
     * 
     * @param type $mesage
     */
    public function setMesage($key, $value) {
        $this->mesage[$key] = $value;
    }

    /**
     * 
     * @return array
     */
    public function getMessage() {

        return $this->mesage;
    }

    /**
     * 
     * @param string $someText
     * @return string
     */
    public function utfToAscii(string $someText): string {//měl by odtranit interpunkci ze zadaných jmen
        $text = iconv('UTF-8', 'ASCII//TRANSLIT', $someText);
        $finalText = preg_replace('/[^a-zA-Z ]/', '', $text);

        return $finalText;
    }

    /**
     * 
     * @param string $year
     * @return bool
     */
    public function controllSpam(string $year): bool {
        if ($year != date('Y') || !isset($year)) {

            $this->setMesage('spam', 'Zadané datum nesouhlasí');

            return TRUE;
        } else {

            return FALSE;
        }
    }

    /**
     * 
     * @param string $pass
     * @param string $aPass
     * @return bool
     */
    public function controllPassword(string $pass, string $aPass): bool {

        if (mb_strlen($pass) < 5 || !isset($pass)) {

            $this->setMesage('password', 'Zadané heslo je krátké');

            return TRUE;
        } elseif ($pass != $aPass || !isset($aPass)) {

            $this->setMesage('password_again', 'Zadaná hesla nesouhlasí');

            return TRUE;
        } else {

            return FALSE;
        }
    }

    /**
     * 
     * @param string $pass
     * @param string $aPass
     * @return bool
     */
    public function controllLogPassword(string $pass, string $aPass): bool {

        if (!password_verify($pass, $aPass) || !isset($pass)) {
            unset($_SESSION['nick']);
            $this->setMesage('logPassword', 'Zadané heslo nesouhlasí');

            return TRUE;
        } else {

            return FALSE;
        }
    }

    /**
     * 
     * @param int $exist
     * @return bool
     */
    public function controllNick(int $exist): bool {
        if ($exist > 0) {
            $this->setMesage('nick', 'Uživatel s tímto nickem již existuje');

            return TRUE;
        } else {

            return FALSE;
        }
    }

    /**
     * 
     * @param int $exist
     * @return bool
     */
    public function controllLogNick(int $exist): bool {

        if ($exist === 0) {
            unset($_SESSION['nick']);
            $this->setMesage('logNick', 'Uživatel s tímto nickem neexistuje');

            return TRUE;
        } else {

            return FALSE;
        }
    }

    /**
     * 
     * @param string $mail
     * @return bool
     */
    public function controllMailAdress(string $mail): bool {

        $reg = '/^\w[\w-.+]*@\w[\w-.+]+\.[a-zA-Z]{2,4}$/';
        if (!preg_match($reg, $mail)) {
            $this->setMesage('mail', 'Zadaná adresa není ve správném formátu');

            return TRUE;
        } else {
            
            return FALSE;
        }
    }

    /**
     * 
     * @param string $name
     * @return bool
     */
    public function controllName(string $name): bool {
        $controlName = $this->utfToAscii($name);
        $reg = '/^[[:upper:]][[:alpha:]]+([[:blank:]][[:upper:]][[:alpha:]]+)+$/';
        if (!preg_match($reg, $controlName)) {
            $this->setMesage('name', 'Zadané jméno není ve správném formátu');

            return TRUE;
        } else {
            
            return FALSE;
        }
    }

}
