<?php
include_once ('api.php');

$router->postControll();
$router->getLogout($id);
$router->getMessage();
$router->getCtrl()->getLogMail();
?>
<!DOCTYPE html>
<html lang='cs'>
    <head>
        <title>
            <?php
            echo $router->getCtrl()->getTitle();
            ?>
        </title>
        <meta charset='utf-8'>
        <meta name='description' content='Mé první stránky'>
        <meta name='keywords' content=''>
        <meta name='author' content='Tomáš Hisem'>
        <meta name='robots' content='all'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
        <link rel="stylesheet" href="muj_styl.css" type="text/css">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">-->
        <link rel="shortcut icon" href="pictures/favicon.ico" type="image/x-icon">
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
        <script src='https://code.jquery.com/jquery-3.2.1.min.js'></script>
        <script src='script.js'></script>
    </head>
    <body>
        <div id='modal-background'></div>
        <div id='modal-popup' class="logModal"><div id="closeModal">X</div><span class="hiden">Zavřít</span><?= $router->getCtrl()->getView()->viewLogForm() ?></div>  
        <div id='modal-popup' class="regModal"><div id="closeModal" class="closeReg">X</div><span class="hiden">Zavřít</span><?= $router->getCtrl()->getView()->viewRegister() ?></div>       
        <div class="podbarveni">
        
        <div id="logo">
            <?php
            echo $router->getCtrl()->getLogo();
            ?>
        </div>
        <div id="login">
            <ul>
                <?php
                echo $router->getCtrl()->getLoginHref();
                ?>  
            </ul>
        </div>
        <article id="obal">
            <header id="hlavicka">

                <nav id="menu">                  
                    <ul>
                        <?php
                        foreach ($router->getCtrl()->getMenu() as $val) {
                            if ((int) $val['id'] < 6 || ((int) $val['id'] === 9 && isset($_SESSION['nick'])) || (int) $val['id'] > 9) {
                                echo $val['link'];
                            }
                        }
                        ?>
                    </ul>
                </nav>
            </header>
            <div id="hiddenMenu"> <button id="menuButton" name="menuButton"><div style="font-size: 10px;text-align: center">MENU</div>≡</button>
        <div id="menuInHiddenMenu" name="hiddenMenu">
            <nav id="">                  
                <ul>
                    <?php
                    foreach ($router->getCtrl()->getMenu() as $val) {
                        if ((int) $val['id'] < 6 || ((int) $val['id'] === 9 && isset($_SESSION['nick'])) || (int) $val['id'] > 9) {
                            echo $val['link'];
                        }
                    }
                    ?>
                </ul>
            </nav>
        </div></div>
            <section id="obsah">
                <?php
                echo $router->getCtrl()->getPage();
                ?>  
            </section> 
            <footer id="pata">
                <p>Stránky vytvořil &copy; TH, e-mail: tomas.hisem&#64;seznam.cz</p>
            </footer>
        </article></div>
    <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>-->
    </body>
</html> 
