<?php

require_once ('api.php');

$logNick = ($_POST['logNick']) ?? '';
$logPassword = ($_POST['logPassword']) ?? '';
$logYear = ($_POST['logYear']) ?? '';

$return = [];
$success = $router->getCtrl()->getModel()->getLogStatus($router->getDataFromForm(), $router->getTextCtrl());

$messages = $router->getTextCtrl()->getMessage();
$return['success'] = $success;
$return['message'] = '';
foreach ($messages as $key => $message) {
    if ($message !== '') {
        $return['message'] = $message;
        $return['key'] = $key;
    }
}

header('Content-type: application/json; charset=utf-8');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

echo json_encode($return);
