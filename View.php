<?php

namespace Page;

class View {

    /**
     *
     * @var array 
     */
    private $message;

    /**
     * 
     * @var string
     */
    private $logMail;

    /**
     * 
     * @param array $message
     */
    public function setMessage(array $message) {
        $this->message = $message;
    }

    /**
     * 
     * @param string $logMail
     */
    public function setLogMail(string $logMail) {
        $this->logMail = $logMail;
    }

    /**
     * 
     * @return array
     */
    function getMessage(): array {
        return $this->message;
    }

    /**
     * 
     * @param string $id
     * @param array $arrayMenuData
     * @param array $arrayHobbies
     * @return string
     */
    public function viewPage(string $id, array $arrayMenuData, array $arrayHobbies, array $arrayProfile): string {
        $string = ['%hobbies%', '%hobbiesMenu%', '%mailForm%', '%login%', '%registration%', '%changeProfile%'];
        $hobbies = $this->viewHobbies($arrayHobbies);
        $newString = [$hobbies['content'], $hobbies['menu'], $this->viewMailForm(), $this->viewLogForm(), $this->viewRegister(), $this->getProfilePage($arrayProfile)];

        $stranka = str_replace($string, $newString, $arrayMenuData[$id]['content']);

        return '<p>' . $stranka . '</p>';
    }

    /**
     * 
     * @param string $id
     * @param array $arrayMenuData
     * @param array $arrayHobbies
     * @param string $success
     * @param array $arrayProfile
     * @return string
     */
    public function viewFinalPage(string $id, array $arrayMenuData, array $arrayHobbies, string $success, array $arrayProfile): string {
        $finalPage = '<h1>' . $arrayMenuData[$id]['menu'] . '</h1>';
        $finalPage .= $this->viewPage($id, $arrayMenuData, $arrayHobbies, $arrayProfile);
        $finalPage .= "<p class='red' style='text-align: center;'>" . $success . "</p>";


        return $finalPage;
    }

    /**
     * 
     * @param array $rows
     * @return array
     * 
     */
    public function viewHobbies(array $rows): array {
        $article = '';
        $url = '';
        $nameHobby = '';
        $link = '';
        $i = 0;
        foreach ($rows as $row) {
            $url = $row['url'];
            $nameHobby .= '<h2 data-hobbyId="#hobby-' . $i . '" >' . $row['nameHobby'] . '</h2>';
            $pathPicture = "pictures/$url.png";
            if (File_Exists($pathPicture)) {
                $pictureView = '<img  src="' . $pathPicture . '" alt = "' . $pathPicture . '">';
            }

            $article = '<p id="hobby-' . $i . '" class="js-hobbyContent">' . ($pictureView ? $pictureView : '') . $row['article'] . '</p>';

            $link .= $article;

            $i++;
        }

        return ['content' => $link,
            'menu' => $nameHobby];
    }

    /**
     * 
     * @return string
     */
    public function viewMailForm(): string {
        $message = $this->message;
        $mailForm = "<form name='emailForm' action='' method='POST'>
        <fieldset>
        <LABEL FOR='email'>Váš e-mail:</LABEL><br>
        <INPUT TYPE='text' ID='email' NAME='emailFromContact' VALUE='' PLACEHOLDER='" . $this->logMail . "'><br>" .
                (!empty($message['mail']) ? '<div class="red">' . $message['mail'] . '</div>' : '' ) .
                "<LABEL FOR='spam'>Napište aktuální rok:</LABEL><br>
        <INPUT TYPE='text' ID='spam' NAME='emailYear' VALUE=''><br>" .
                (!empty($message['spam']) ? '<div class="red">' . $message['spam'] . '</div>' : '') .
                "<LABEL FOR='text'>Zde napište svou zprávu:</LABEL><br>
        <TEXTAREA ID='text' NAME='emailText' rows='5' cols='40'></TEXTAREA><br>" .
                (!empty($message['emailText']) ? '<div class="red">' . $message['emailText'] . '</div>' : '') .
                "<INPUT type='SUBMIT' name='submit' value='Odešli e-mail'>
        </fieldset>
        </form>";

        return $mailForm;
    }

    /**
     * 
     * @param string $formStr
     * @return array
     */
    private function getWarning(string $formStr): array {
        $pattern = "/(NAME ?= ?' ?)([[:alpha:]]+)/";
        preg_match_all($pattern, $formStr, $matches);
        $arrayWarning = [];
        for ($i = 0; $i < count($matches[2]); $i++) {
            $arrayWarning[$i] = "(!empty(\$message['" . $matches[2][$i] . "']" . ") ? ";
            $arrayWarning[$i] .= htmlspecialchars("'<div class=\"red\">'");
            $arrayWarning[$i] .= htmlspecialchars(". \$message['" . $matches[2][$i] . "'].  '</div>' : '') . \"<br>\"");
        }
        return $arrayWarning;
    }

    /**
     * 
     * @return string
     */
    public function viewRegister(): string {
        $message = $this->message;
        $register = "<FORM name = 'user' action = '' method = 'POST'>
        <FIELDSET>
                <LABEL for='regName1'>Jméno a příjmení:</LABEL><BR>
        <INPUT TYPE = 'text' ID='regName1' NAME = 'firstName' VALUE ='' PLACEHOLDER= 'Jméno Příjmení'><br>" .
                (!empty($message['name']) ? '<div class="red">' . $message['name'] . '</div>' : '') .
                "<LABEL for='regNick'>Nick:</LABEL><BR>
        <INPUT TYPE = 'text' ID='regNick' NAME = 'nick' VALUE = '' PLACEHOLDER='Přihlašovací jméno'><br>" .
                (!empty($message['nick']) ? '<div class="red">' . $message['nick'] . '</div>' : '') .
                "<LABEL for='regMail'>E-mail:</LABEL><BR>
        <INPUT TYPE = 'text' ID='regMail' NAME = 'mail' VALUE = '' PLACEHOLDER='@'><br>" .
                (!empty($message['mail']) ? '<div class="red">' . $message['mail'] . '</div>' : '') .
                "<LABEL for='regPass1'>Heslo(min délka 5 znaků):</LABEL><BR>
        <INPUT TYPE = 'password' ID='regPass1' NAME = 'password' VALUE = ''><br>" .
                (!empty($message['password']) ? '<div class="red">' . $message['password'] . '</div>' : '') .
                "<LABEL for='regPass2'>Ověření hesla:</LABEL><BR>
        <INPUT TYPE = 'password' ID='regPass2' NAME = 'password_again' VALUE = ''><br>" .
                (!empty($message['password_again']) ? '<div class="red">' . $message['password_again'] . '</div>' : '') .
                "<LABEL for='regSpam'>Zadej současný rok:</LABEL><BR>
        <INPUT TYPE = 'text' ID='regSpam' NAME = 'year' VALUE = '' PLACEHOLDER='Antispam'><br>" .
                (!empty($message['spam']) ? '<div class="red">' . $message['spam'] . '</div>' : '') .
                "<INPUT TYPE = 'submit' NAME='registryForm' id='submitReg' VALUE = 'Odešli'>
        <a href = 'index.php?id=1'>Zpět</a>
        </FIELDSET>
        </FORM>";

        return $register;
    }

    /**
     * 
     * @return string
     */
    public function viewLogForm(): string {
        $message = $this->message;
        $logForm = "<FORM name = 'login' action = '' method = 'POST'>
        <FIELDSET>
        <LABEL for='logNick'>Nick:</LABEL><BR>
        <INPUT TYPE = 'text' ID='logNick' NAME = 'logNick' VALUE ='' PLACEHOLDER='Přihlašovací jméno'><br>" .
                (!empty($message['logNick']) ? '<div class="red">' . $message['logNick'] . '</div>' : '') .
                "<LABEL for='logPass1'>Heslo:</LABEL><BR>
        <INPUT TYPE = 'password' ID='logPass1' NAME = 'logPassword' VALUE = ''><br>" .
                (!empty($message['logPassword']) ? '<div class="red">' . $message['logPassword'] . '</div>' : '') .
                "<LABEL for='logSpam'>Zadej současný rok:</LABEL><BR>
        <INPUT TYPE = 'text' ID='logSpam' NAME = 'logYear' VALUE ='' PLACEHOLDER= 'Antispam'><br>" .
                (!empty($message['spam']) ? '<div class="red">' . $message['spam'] . '</div>' : '') .
                "<INPUT TYPE = 'submit' id='submitLog' VALUE = 'Odešli'>
        <a href = 'index.php?id=1'>Zpět</a>
        </FIELDSET>
        </FORM>";

        return $logForm;
    }

    /**
     * 
     * @param type $logUser
     * @return string
     */
    public function viewLogo($logUser): string {

        return '<img src="pictures/' . $logUser . '.png" alt="avatar' . $logUser . '"><h2>' . $logUser . '</h2>';
    }

    /**
     * 
     * @param array $row
     * @return array
     */
    public function viewLogin(): array {
        $return = [
            'login' => '<a id="myModalNew" href="index.php?id=6"><li>Přihlásit se</li></a>',
            'registration' => '<a id="myModalNew" class="registration" href="index.php?id=7"><li>Registrovat</li></a>',
        ];

        return $return;
    }

    /**
     * 
     * @param array $row
     * @return string
     */
    public function viewLogout(): string {

        return '<a id="menu" href="index.php?id=8"><li>Odhlásit se</li></a>';
    }

    public function getProfilePage($data) {
        $profile = '<div id="myProfile" style="text-align: center">' .
                $this->nameForm($data) . $this->nickForm($data) . $this->mailForm($data) .
                $this->passwordForm() . $this->deleteUser() .
                '</div>';

        return $profile;
    }

    public function nameForm($data) {

        return '<form id="firstNameForm" name="firstNameForm" method="POST" action=""> 
                <label id="labelFNP" for="firstNameProfile">Jméno: ' . (($data["name"]) ?? '') . ' </label><br>
                <input type="text" id="firstNameProfile" name="firstNameProfile" value="" >
                <button type´"submit" name="btnFNP" id="btnFNP">Změnit</button></form>';
    }

    public function nickForm($data) {

        return '<form id="nickForm" name="nickForm" method="POST" action="">
                <label for="nickProfile">NICK: ' . ($data["nick"] ?? '') . '</label><br>
                <input type="text" id="nickProfile" name="nickProfile" value="">
                <button type´"submit" name="btnNP" id="btnNP">Změnit</button></form>';
    }

    public function mailForm($data) {

        return '<form id="mailForm" name="mailForm" method="POST" action="">
                <label for="mailProfile">MAIL: ' . ($data["mail"] ?? '') . '</label><br>
                <input type="text" id="mailProfile" name="mailProfile" value="">
                <button type´"submit" name="btnMP" id="btnMP">Změnit</button></form>';
    }

    public function passwordForm() {

        return '<form id="passwordForm" name="passwordForm" method="POST" action="">
                <label id="passwordProfileLabel" for="passwordProfile" >HESLO: </label><br>
                <input type="password" id="passwordProfile" name="passwordProfile" value=""><br>
                <label id="password_againProfileLabel" for="password_againProfile" >Heslo znova: </label><br>
                <input type="password" id="password_againProfile" name="password_againProfile" value="">
                <button type´"submit" name="btnPP" id="btnPP">Změnit</button></form>';
    }

    public function deleteUser() {
        return '<form id="deleteUser" name="deleteUser" method"POST" action="">'
                . '<button type="submit" id="subDeleteUser">Smazat celý účet</button>'
                . '</form>';
    }

}
