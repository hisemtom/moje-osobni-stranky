<?php

include_once ('api.php');

$firstName = ($_POST['firstName']) ?? '';
$lastName = ($_POST['lastName']) ?? '';
$nick = ($_POST['nick']) ?? '';
$mail = ($_POST['mail']) ?? '';
$password = ($_POST['password']) ?? '';
$password_again = ($_POST['password_again']) ?? '';
$spam = ($_POST['spam']) ?? '';

$return = [];
$success = $router->getCtrl()->getModel()->getReg($router->getDataFromForm(), $router->getTextCtrl());

$messages = $router->getTextCtrl()->getMessage();
$return['success'] = $success;
$return['message'] = '';
foreach ($messages as $key => $message) {
    if ($message !== '') {
        $return['message'] = $message;
        $return['key'] = $key;
    }
}

header('Content-type: application/json; charset=utf-8');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

echo json_encode($return);

